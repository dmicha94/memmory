﻿namespace Memmory
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.card1 = new System.Windows.Forms.PictureBox();
            this.card2 = new System.Windows.Forms.PictureBox();
            this.card4 = new System.Windows.Forms.PictureBox();
            this.card3 = new System.Windows.Forms.PictureBox();
            this.card6 = new System.Windows.Forms.PictureBox();
            this.card5 = new System.Windows.Forms.PictureBox();
            this.card8 = new System.Windows.Forms.PictureBox();
            this.card7 = new System.Windows.Forms.PictureBox();
            this.card12 = new System.Windows.Forms.PictureBox();
            this.card11 = new System.Windows.Forms.PictureBox();
            this.card10 = new System.Windows.Forms.PictureBox();
            this.card9 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.card1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.card9)).BeginInit();
            this.SuspendLayout();
            // 
            // card1
            // 
            this.card1.BackColor = System.Drawing.Color.SandyBrown;
            this.card1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card1.Location = new System.Drawing.Point(86, 46);
            this.card1.Name = "card1";
            this.card1.Size = new System.Drawing.Size(142, 139);
            this.card1.TabIndex = 0;
            this.card1.TabStop = false;
            this.card1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // card2
            // 
            this.card2.BackColor = System.Drawing.Color.SandyBrown;
            this.card2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card2.Location = new System.Drawing.Point(234, 46);
            this.card2.Name = "card2";
            this.card2.Size = new System.Drawing.Size(142, 139);
            this.card2.TabIndex = 1;
            this.card2.TabStop = false;
            // 
            // card4
            // 
            this.card4.BackColor = System.Drawing.Color.SandyBrown;
            this.card4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card4.Location = new System.Drawing.Point(530, 46);
            this.card4.Name = "card4";
            this.card4.Size = new System.Drawing.Size(142, 139);
            this.card4.TabIndex = 3;
            this.card4.TabStop = false;
            // 
            // card3
            // 
            this.card3.BackColor = System.Drawing.Color.SandyBrown;
            this.card3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card3.Location = new System.Drawing.Point(382, 46);
            this.card3.Name = "card3";
            this.card3.Size = new System.Drawing.Size(142, 139);
            this.card3.TabIndex = 2;
            this.card3.TabStop = false;
            // 
            // card6
            // 
            this.card6.BackColor = System.Drawing.Color.SandyBrown;
            this.card6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card6.Location = new System.Drawing.Point(234, 191);
            this.card6.Name = "card6";
            this.card6.Size = new System.Drawing.Size(142, 139);
            this.card6.TabIndex = 5;
            this.card6.TabStop = false;
            // 
            // card5
            // 
            this.card5.BackColor = System.Drawing.Color.SandyBrown;
            this.card5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card5.Location = new System.Drawing.Point(86, 191);
            this.card5.Name = "card5";
            this.card5.Size = new System.Drawing.Size(142, 139);
            this.card5.TabIndex = 4;
            this.card5.TabStop = false;
            // 
            // card8
            // 
            this.card8.BackColor = System.Drawing.Color.SandyBrown;
            this.card8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card8.Location = new System.Drawing.Point(530, 191);
            this.card8.Name = "card8";
            this.card8.Size = new System.Drawing.Size(142, 139);
            this.card8.TabIndex = 7;
            this.card8.TabStop = false;
            // 
            // card7
            // 
            this.card7.BackColor = System.Drawing.Color.SandyBrown;
            this.card7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card7.Location = new System.Drawing.Point(382, 191);
            this.card7.Name = "card7";
            this.card7.Size = new System.Drawing.Size(142, 139);
            this.card7.TabIndex = 6;
            this.card7.TabStop = false;
            // 
            // card12
            // 
            this.card12.BackColor = System.Drawing.Color.SandyBrown;
            this.card12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card12.Location = new System.Drawing.Point(530, 336);
            this.card12.Name = "card12";
            this.card12.Size = new System.Drawing.Size(142, 139);
            this.card12.TabIndex = 11;
            this.card12.TabStop = false;
            // 
            // card11
            // 
            this.card11.BackColor = System.Drawing.Color.SandyBrown;
            this.card11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card11.Location = new System.Drawing.Point(382, 336);
            this.card11.Name = "card11";
            this.card11.Size = new System.Drawing.Size(142, 139);
            this.card11.TabIndex = 10;
            this.card11.TabStop = false;
            // 
            // card10
            // 
            this.card10.BackColor = System.Drawing.Color.SandyBrown;
            this.card10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card10.Location = new System.Drawing.Point(234, 336);
            this.card10.Name = "card10";
            this.card10.Size = new System.Drawing.Size(142, 139);
            this.card10.TabIndex = 9;
            this.card10.TabStop = false;
            // 
            // card9
            // 
            this.card9.BackColor = System.Drawing.Color.SandyBrown;
            this.card9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card9.Location = new System.Drawing.Point(86, 336);
            this.card9.Name = "card9";
            this.card9.Size = new System.Drawing.Size(142, 139);
            this.card9.TabIndex = 8;
            this.card9.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(325, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ilość ruchów: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(784, 523);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.card12);
            this.Controls.Add(this.card11);
            this.Controls.Add(this.card10);
            this.Controls.Add(this.card9);
            this.Controls.Add(this.card8);
            this.Controls.Add(this.card7);
            this.Controls.Add(this.card6);
            this.Controls.Add(this.card5);
            this.Controls.Add(this.card4);
            this.Controls.Add(this.card3);
            this.Controls.Add(this.card2);
            this.Controls.Add(this.card1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.card1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.card9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox card1;
        private System.Windows.Forms.PictureBox card2;
        private System.Windows.Forms.PictureBox card4;
        private System.Windows.Forms.PictureBox card3;
        private System.Windows.Forms.PictureBox card6;
        private System.Windows.Forms.PictureBox card5;
        private System.Windows.Forms.PictureBox card8;
        private System.Windows.Forms.PictureBox card7;
        private System.Windows.Forms.PictureBox card12;
        private System.Windows.Forms.PictureBox card11;
        private System.Windows.Forms.PictureBox card10;
        private System.Windows.Forms.PictureBox card9;
        private System.Windows.Forms.Label label1;
    }
}

